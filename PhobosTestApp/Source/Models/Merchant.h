//
//  Merchant.h
//  PhobosTestApp
//
//  Created by Александр Сенченков on 11.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Merchant : NSObject <NSCoding>

@property (nonatomic, strong) NSString *merchantID;
@property (nonatomic, strong) NSString *merchantName;

-(void)fillMerchantWithParam:(NSDictionary *)param;

@end
