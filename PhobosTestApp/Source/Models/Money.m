//
//  Money.m
//  PhobosTestApp
//
//  Created by Александр Сенченков on 11.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import "Money.h"

@implementation Money

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    if (self) {
        
        self.moneyAmount       = [aDecoder decodeObjectForKey:@"amount"];
        self.moneyCurrencyCode = [aDecoder decodeObjectForKey:@"curerncy_code"];

    }
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.moneyAmount forKey:@"amount"];
    
    [aCoder encodeObject:self.moneyCurrencyCode forKey:@"curerncy_code"];
    
}

-(instancetype)init
{
    self = [super init];
    
    if (self) {
        
        self.moneyAmount        = @"";
        self.moneyCurrencyCode  = @"";
    }
    
    return self;
}

-(void)fillMoneyWithParam:(NSDictionary *)param
{
    if (param[@"amount"] && ![param isKindOfClass:[NSNull class]]) {
        
        self.moneyAmount = [NSString stringWithFormat:@"%@", param[@"amount"]];
    }
    
    if (param[@"curerncy_code"] && ![param isKindOfClass:[NSNull class]]) {
        
        self.moneyCurrencyCode = [NSString stringWithFormat:@"%@", param[@"curerncy_code"]];
    }
}
@end
