//
//  Category.m
//  PhobosTestApp
//
//  Created by Александр Сенченков on 11.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import "Category.h"

@implementation Category

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    if (self) {
        self.categoryID          = [aDecoder decodeObjectForKey:@"id"];
        self.categoryIcon        = [aDecoder decodeObjectForKey:@"icon"];
        self.categoryName        = [aDecoder decodeObjectForKey:@"name"];
        self.categorySubIcon     = [aDecoder decodeObjectForKey:@"sub_icon"];
        self.categoryDisplayName = [aDecoder decodeObjectForKey:@"display_name"];

    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.categoryID forKey:@"id"];
    
    [aCoder encodeObject:self.categoryIcon forKey:@"icon"];
    
    [aCoder encodeObject:self.categoryName forKey:@"name"];
    
    [aCoder encodeObject:self.categorySubIcon forKey:@"sub_icon"];
    
    [aCoder encodeObject:self.categoryDisplayName forKey:@"display_name"];
    
    
}

-(instancetype)init
{
    self = [super init];
    
    if (self) {
     
        self.categoryID             = @"";
        self.categoryIcon           = @"";
        self.categoryName           = @"";
        self.categorySubIcon        = @"";
        self.categoryDisplayName    = @"";
    }
    
    return self;
}

-(void)fillCategoryWithParam:(NSDictionary *)param
{
    if (param[@"id"] && ![param isKindOfClass:[NSNull class]]) {
        
        self.categoryID = [NSString stringWithFormat:@"%@", param[@"id"]];
    }
    
    if (param[@"name"] && ![param isKindOfClass:[NSNull class]]) {
        
        self.categoryName = [NSString stringWithFormat:@"%@", param[@"name"]];
    }
    
    if (param[@"display_name"] && ![param isKindOfClass:[NSNull class]]) {
        
        self.categoryDisplayName = [NSString stringWithFormat:@"%@", param[@"display_name"]];
    }
    
    if (param[@"icon"] && ![param isKindOfClass:[NSNull class]]) {
        
        self.categoryIcon = [NSString stringWithFormat:@"%@", param[@"icon"]];
    }
    
    if (param[@"sub_icon"] && ![param isKindOfClass:[NSNull class]]) {
        
        self.categorySubIcon = [NSString stringWithFormat:@"%@", param[@"sub_icon"]];
    }
    
}
@end
