//
//  Money.h
//  PhobosTestApp
//
//  Created by Александр Сенченков on 11.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Money : NSObject <NSCoding>

@property (nonatomic, strong) NSString *moneyAmount;
@property (nonatomic, strong) NSString *moneyCurrencyCode;

-(void)fillMoneyWithParam:(NSDictionary *)param;

@end
