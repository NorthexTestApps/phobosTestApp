//
//  ErrorObj.h
//  Informator
//
//  Created by Александр Сенченков on 11.11.16.
//  Copyright © 2016 Александр Сенченков. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ErrorObj : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, assign) NSInteger code;
@end
