//
//  Merchant.m
//  PhobosTestApp
//
//  Created by Александр Сенченков on 11.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import "Merchant.h"

@implementation Merchant

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    if (self) {
        self.merchantID       = [aDecoder decodeObjectForKey:@"id"];
        self.merchantName = [aDecoder decodeObjectForKey:@"name"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.merchantID forKey:@"id"];
    
    [aCoder encodeObject:self.merchantName forKey:@"name"];
    
}

-(instancetype)init
{
    self = [super init];
    
    if (self) {
        
        self.merchantID   = @"";
        self.merchantName = @"";
    }
    
    return self;
}

-(void)fillMerchantWithParam:(NSDictionary *)param
{
    if (param[@"id"] && ![param isKindOfClass:[NSNull class]]) {
        
        self.merchantID = [NSString stringWithFormat:@"%@", param[@"id"]];
    }
    
    if (param[@"name"] && ![param isKindOfClass:[NSNull class]]) {
        
        self.merchantName = [NSString stringWithFormat:@"%@", param[@"name"]];
    }
    
}
@end
