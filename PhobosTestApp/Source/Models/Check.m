//
//  Check.m
//  PhobosTestApp
//
//  Created by Александр Сенченков on 11.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import "Check.h"

@implementation Check

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    if (self) {
        
        self.checkID            = [aDecoder decodeObjectForKey:@"id"];
        self.checkMoney         = [aDecoder decodeObjectForKey:@"money"];
        self.checkStatus        = [aDecoder decodeObjectForKey:@"status"];
        self.checkComment       = [aDecoder decodeObjectForKey:@"comment"];
        self.checkDetails       = [aDecoder decodeObjectForKey:@"details"];
        self.checkCategory      = [aDecoder decodeObjectForKey:@"category"];
        self.checkMerchant      = [aDecoder decodeObjectForKey:@"merchant"];
        self.checkMimimiles     = [aDecoder decodeObjectForKey:@"mimimiles"];
        self.checkHappenedAt    = [aDecoder decodeObjectForKey:@"happened_at"];
        self.checkDisplayMoney  = [aDecoder decodeObjectForKey:@"display_money"];
    }
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.checkID forKey:@"id"];
    
    [aCoder encodeObject:self.checkMoney forKey:@"money"];
    
    [aCoder encodeObject:self.checkStatus forKey:@"status"];
    
    [aCoder encodeObject:self.checkComment forKey:@"comment"];
    
    [aCoder encodeObject:self.checkDetails forKey:@"details"];
    
    [aCoder encodeObject:self.checkCategory forKey:@"category"];
    
    [aCoder encodeObject:self.checkMerchant forKey:@"merchant"];
    
    [aCoder encodeObject:self.checkMimimiles forKey:@"mimimiles"];
    
    [aCoder encodeObject:self.checkHappenedAt forKey:@"happened_at"];
    
    [aCoder encodeObject:self.checkDisplayMoney forKey:@"display_money"];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.checkHappenedAt    = @"";
        self.checkID            = @"";
        self.checkMoney         = [[Money alloc] init];
        self.checkStatus        = @"";
        self.checkComment       = @"";
        self.checkDetails       = @"";
        self.checkCategory      = [[Category alloc] init];
        self.checkMerchant      = [[Merchant alloc] init];
        self.checkMimimiles     = @"";
        self.checkDisplayMoney  = [[Money alloc] init];
    }
    return self;
}

-(void)fillCheckWithParam:(NSDictionary *)param
{
    if (param[@"id"] && ![param isKindOfClass:[NSNull class]]) {
        
        self.checkID = [NSString stringWithFormat:@"%@", param[@"id"]];
    }
    
    if (param[@"status"] && ![param isKindOfClass:[NSNull class]]) {
        
        self.checkStatus = [NSString stringWithFormat:@"%@", param[@"status"]];
    }
    
    if (param[@"happened_at"] && ![param isKindOfClass:[NSNull class]]) {
        
        self.checkHappenedAt = [NSString stringWithFormat:@"%@", param[@"happened_at"]];
    }
    
    if (param[@"details"] && ![param isKindOfClass:[NSNull class]]) {
        
        self.checkDetails = [NSString stringWithFormat:@"%@", param[@"details"]];
    }
    
    if (param[@"comment"] && ![param isKindOfClass:[NSNull class]]) {
        
        self.checkComment = [NSString stringWithFormat:@"%@", param[@"comment"]];
    }
    
    if (param[@"mimimiles"] && ![param isKindOfClass:[NSNull class]]) {
        
        self.checkMimimiles = [NSString stringWithFormat:@"%@", param[@"mimimiles"]];
    }
    
    if (param[@"mimimiles"] && ![param isKindOfClass:[NSNull class]]) {
        
        self.checkMimimiles = [NSString stringWithFormat:@"%@", param[@"mimimiles"]];
    }
    
    if ([param[@"display_money"] isKindOfClass:[NSDictionary class]]) {
        
        [self.checkDisplayMoney fillMoneyWithParam:param[@"display_money"]];
    }
    
    if ([param[@"money"] isKindOfClass:[NSDictionary class]]) {
        
        [self.checkMoney fillMoneyWithParam:param[@"money"]];
    }
    
    if ([param[@"merchant"] isKindOfClass:[NSDictionary class]]) {
        
        [self.checkMerchant fillMerchantWithParam:param[@"merchant"]];
    }
    
    if ([param[@"category"] isKindOfClass:[NSDictionary class]]) {
        
        [self.checkCategory fillCategoryWithParam:param[@"category"]];
    }

    
}
@end
