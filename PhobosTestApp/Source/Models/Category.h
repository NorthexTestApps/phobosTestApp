//
//  Category.h
//  PhobosTestApp
//
//  Created by Александр Сенченков on 11.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Category : NSObject <NSCoding>

@property (nonatomic, strong) NSString *categoryID;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) NSString *categoryDisplayName;
@property (nonatomic, strong) NSString *categoryIcon;
@property (nonatomic, strong) NSString *categorySubIcon;

-(void)fillCategoryWithParam:(NSDictionary *)param;

@end
