//
//  Check.h
//  PhobosTestApp
//
//  Created by Александр Сенченков on 11.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Money.h"
#import "Merchant.h"
#import "Category.h"

@interface Check : NSObject <NSCoding>

@property (nonatomic, strong) NSString  *checkID;
@property (nonatomic, strong) NSString  *checkStatus;
@property (nonatomic, strong) NSString  *checkHappenedAt;
@property (nonatomic, strong) NSString  *checkDetails;
@property (nonatomic, strong) NSString  *checkComment;
@property (nonatomic, strong) NSString  *checkMimimiles;
@property (nonatomic, strong) Merchant  *checkMerchant;
@property (nonatomic, strong) Money     *checkMoney;
@property (nonatomic, strong) Money     *checkDisplayMoney;
@property (nonatomic, strong) Category  *checkCategory;

-(void)fillCheckWithParam:(NSDictionary *)param;

@end
