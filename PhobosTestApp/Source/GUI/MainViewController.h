//
//  MainViewController.h
//  PhobosTestApp
//
//  Created by Александр Сенченков on 11.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FZAccordionTableView.h>
#import "Check.h"

@interface MainViewController : UIViewController

@property (weak, nonatomic) IBOutlet FZAccordionTableView *tableView;

@end
