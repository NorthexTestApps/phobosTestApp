//
//  MainViewController.m
//  PhobosTestApp
//
//  Created by Александр Сенченков on 11.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import "MainViewController.h"
#import "MainHeadTableViewCell.h"
#import "MainTableViewCell.h"

static NSString *const kTableViewCellReuseIdentifier = @"TableViewCellReuseIdentifier";

static NSString *const savedKey = @"dataSaveServer";

@interface MainViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *sectionData;

@property (nonatomic, retain) UIRefreshControl *refreshControl;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.estimatedRowHeight = 64.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    // Do any additional setup after loading the view.
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kTableViewCellReuseIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"MainTableViewCell" bundle:nil] forCellReuseIdentifier:@"MainTableViewCell"];
    // self.tableView.separatorColor = [UIColor clearColor];
    // Do any additional setup after loading the view.
    self.tableView.allowMultipleSectionsOpen = YES;

    UIView *refreshView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 0, 0)];
    [self.tableView insertSubview:refreshView atIndex:0]; //the tableView is a IBOutlet
    _refreshControl = [[UIRefreshControl alloc] init];
    _refreshControl.tintColor = [UIColor colorWithRed:22.0f/255.0f green:141.0f/255.0f blue:226.0f/255.0f alpha:255.0f/255.0f];
    [_refreshControl addTarget:self action:@selector(reloadDatas) forControlEvents:UIControlEventValueChanged];
    [refreshView addSubview:_refreshControl];

    if ([[NSUserDefaults standardUserDefaults] objectForKey:savedKey]) {
        
        NSData *savedData = [[NSUserDefaults standardUserDefaults] objectForKey:savedKey];
        
        self.sectionData =  [NSKeyedUnarchiver unarchiveObjectWithData:savedData];
        
        [self.tableView reloadData];
    }
    [self getDataFromServer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)reloadDatas
{
    [self getDataFromServer];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _sectionData.count;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *check = [self.sectionData objectAtIndex:section][@"checks"];
    
    return check.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kDefaultAccordionHeaderViewHeight;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
    return [self tableView:tableView heightForHeaderInSection:section];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0f;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    MainHeadTableViewCell *headCell = [MainHeadTableViewCell new];
    
    headCell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kAccordionHeaderViewReuseIdentifier];
    
    if (headCell == nil)
        
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MainHeadTableViewCell" owner:self options:nil];
        headCell = [nib objectAtIndex:0];
        
    }

    headCell.labelDate.text = [self.sectionData objectAtIndex:section][@"date"];
    
    return headCell;
}



#pragma mark - <FZAccordionTableViewDelegate> -

- (void)tableView:(FZAccordionTableView *)tableView willOpenSection:(NSInteger)section withHeader:(MainHeadTableViewCell *)header {
    
    [header.imageViewArrow setImage:[UIImage imageNamed:@"arrowUp"]];
    
}

- (void)tableView:(FZAccordionTableView *)tableView didOpenSection:(NSInteger)section withHeader:(MainHeadTableViewCell *)header {
    
    [header.imageViewArrow setImage:[UIImage imageNamed:@"arrowUp"]];
    
}

- (void)tableView:(FZAccordionTableView *)tableView willCloseSection:(NSInteger)section withHeader:(MainHeadTableViewCell *)header {
    
    [header.imageViewArrow setImage:[UIImage imageNamed:@"arrowDown"]];
    
}

- (void)tableView:(FZAccordionTableView *)tableView didCloseSection:(NSInteger)section withHeader:(MainHeadTableViewCell *)header {
    
    [header.imageViewArrow setImage:[UIImage imageNamed:@"arrowDown"]];
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MainTableViewCell"];
    
    NSArray *check = [self.sectionData objectAtIndex:indexPath.section][@"checks"];
    
    cell.check = [check objectAtIndex:indexPath.row];
    
    return cell;
}

-(void)getDataFromServer
{
    [self showHUD];
    
    __weak typeof(self) weakSelf = self;
    
    [[NXConnectionManager instance] getTestDataWithParam:nil callback:^(id response, ErrorObj *error) {
        
        [weakSelf hideHUD];
        
        [_refreshControl endRefreshing];
        
        if (response) {
            
            if (!self.sectionData) {
                
                self.sectionData = [NSMutableArray new];
            }
            
            [self.sectionData removeAllObjects];
            
            NSDictionary *datas = response[@"feed"];
            
            if ([datas isKindOfClass:[NSDictionary class]]) {
                
                NSArray *keys = [datas allKeys];
                
                NSArray *sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id a1, id a2) {
                    NSString *s1 = [NSString stringWithFormat:@"%@",a1];
                    NSString *s2 = [NSString stringWithFormat:@"%@",a2];
                    return [s1 compare:s2];
                }];
                
                [sortedArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                    NSMutableArray *checks = [NSMutableArray new];
                    
                    NSArray *checkArr = response[@"feed"][obj];
                    
                    NSArray *sortedArrayCheck = [checkArr sortedArrayUsingComparator:^NSComparisonResult(id a1, id a2) {
                        NSString *s1 = [NSString stringWithFormat:@"%@",a1[@"happened_at"]];
                        NSString *s2 = [NSString stringWithFormat:@"%@",a2[@"happened_at"]];
                        return [s1 compare:s2];
                    }];
                    
                    if (sortedArrayCheck.count > 0) {
                        
                        [sortedArrayCheck enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                           
                            Check *check = [[Check alloc] init];
                            [check fillCheckWithParam:obj];
                            [checks addObject:check];
                        }];
                        
                        [self.sectionData addObject:@{@"date" : obj, @"checks" : checks}];
                    }
                }];
                
                NSData *dataEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:self.sectionData];
                
                [[NSUserDefaults standardUserDefaults] setObject:dataEncodedObject forKey:savedKey];
                
                [self.tableView reloadData];
            }
            NSLog(@"%@", self.sectionData);
        }
    }];
}
@end
