//
//  MainTableViewCell.m
//  PhobosTestApp
//
//  Created by Александр Сенченков on 11.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import "MainTableViewCell.h"

@implementation MainTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(NSString *) toLocalTime:(NSString *)dateUTC
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:locale];
    [dateFormatter setDateFormat:@"dd.MM.yyyy, HH:mm"];
    NSTimeInterval epoch = [dateUTC doubleValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:epoch];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    //NSLog(@"formattedDateString: %@", formattedDateString);
    
    return formattedDateString;
}

-(void)setCheck:(Check *)check
{
    _check = check;
    
    self.labelTitle.text = check.checkDetails;
    self.labelCategory.text = check.checkCategory.categoryName;
    self.commentLabel.text = check.checkComment;
    self.currencyLabel.text = check.checkDisplayMoney.moneyCurrencyCode;
    self.countCurrency.text = check.checkDisplayMoney.moneyAmount;
    self.dateLabel.text = [self toLocalTime:check.checkHappenedAt];
}
@end
