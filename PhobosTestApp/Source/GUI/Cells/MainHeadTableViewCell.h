//
//  MainHeadTableViewCell.h
//  PhobosTestApp
//
//  Created by Александр Сенченков on 11.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import <FZAccordionTableView/FZAccordionTableView.h>

static const CGFloat kDefaultAccordionHeaderViewHeight = 60.0f;
static NSString *const kAccordionHeaderViewReuseIdentifier = @"AccordionHeaderViewReuseIdentifier";
@interface MainHeadTableViewCell : FZAccordionTableViewHeaderView
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewArrow;
@end
