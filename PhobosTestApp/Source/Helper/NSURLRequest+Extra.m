//
//  NSURLRequest+Extra.m
//  Cubux
//
//  Created by spens on 10/06/16.
//  Copyright © 2016 Bern. All rights reserved.
//

#import "NSURLRequest+Extra.h"

@implementation NSURLRequest (Extra)

- (void)logRequest
{
    __block NSMutableString* displayString = [NSMutableString stringWithFormat:@"curl -X %@", self.HTTPMethod];
    [displayString appendFormat:@" \'%@\'",  self.URL.absoluteString];
    
    [self.allHTTPHeaderFields enumerateKeysAndObjectsUsingBlock:^(id key, id val, BOOL *stop) {
        [displayString appendFormat:@" -H \'%@: %@\'", key, val];
    }];
    
    if ([self.HTTPMethod isEqualToString:@"POST"] || [self.HTTPMethod isEqualToString:@"PUT"] || [self.HTTPMethod isEqualToString:@"PATCH"])
    {
        if (self.HTTPBody) {
            NSString *params = [[NSString alloc] initWithData:self.HTTPBody encoding:NSUTF8StringEncoding];
            if (params && params.length > 0) {
                NSString *result = [params stringByReplacingOccurrencesOfString:@"+" withString:@" "];
                result = [result stringByRemovingPercentEncoding];
                [displayString appendFormat:@" -F \'%@\'", result];
            }
        }
    }
#ifdef DEBUG
    NSLog(@"%@", displayString);
#endif
}

@end
