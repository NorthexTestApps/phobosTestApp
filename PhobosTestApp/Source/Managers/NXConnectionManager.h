//
//  IRConnectionManager.h
//  Informator
//
//  Created by Александр Сенченков on 11.11.16.
//  Copyright © 2016 Александр Сенченков. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import "Constants.h"


@interface NXConnectionManager : AFHTTPSessionManager

- (NSError *) noConnectionError;

+ (NXConnectionManager *)instance;

// Get all data
- (void)getTestDataWithParam:(NSDictionary *)params callback:(Callback) callback;



@end
